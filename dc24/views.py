from collections import defaultdict
from datetime import timedelta
from decimal import Decimal

from django.contrib.auth import get_user_model
from django.conf import settings
from django.http import JsonResponse, HttpResponse
from django.shortcuts import render
from django.utils import timezone
from django.utils.text import slugify

from wafer.schedule.models import ScheduleItem

from .forms import (
    VisaRequestPassportDetailsForm,
    VisaRequestInviteeDetailsForm,
    VisaRequestTravelDetailsForm,
    VisaRequestGuaranteeDetailsForm,
)

from bursary.models import Bursary
from invoices.models import Invoice
from register.models import Attendee, Queue
from register.views import STEPS


def now_or_next(request, venue_id):
    talk_now = False
    talk_next = False
    reload_seconds = 60
    talk = None

    now = timezone.now()
    # now
    try:
        item = ScheduleItem.objects.filter(
            venue=venue_id,
            talk__isnull=False,
            slots__start_time__lte=now,
            slots__end_time__gte=now,
        ).order_by("slots__start_time")[0]
        duration = item.get_duration_minutes()
        end = item.get_start_datetime() + timedelta(minutes=duration)
        reload_seconds = (end - timezone.now()).total_seconds()

        talk_now = True
        talk = item.talk_id
    except IndexError:
        pass

    if not talk:
        # next
        try:
            item = ScheduleItem.objects.filter(
                venue=venue_id,
                talk__isnull=False,
                slots__start_time__gte=now
            ).order_by("slots__start_time")[0]
            reload_seconds = (item.get_start_datetime() - timezone.now()).total_seconds()
            talk_next = True
            talk = item.talk_id
        except IndexError:
            pass

    return JsonResponse({
        "now": talk_now,
        "next": talk_next,
        "reload_seconds": reload_seconds,
        "talk": talk,
    })

def invitation_request_form(request):
    if request.user.is_authenticated:
        email_body_prefix = f"Hello, I'm {request.user.get_full_name()}({request.user.username}). And I'm requesting visa invitation to attend DebConf with following details:"
    else:
        email_body_prefix = f"Hello. I'm requesting visa invitation to attend DebConf with following details:"
    context = {
        "title": "Submit Visa Invitation Request",
        "is_login_required": True,
        "email_title_js_template":"`DC24Visa: ${document.getElementsByName('surname')[0].value}/${document.getElementsByName('givennames')[0].value}`",
        "email_body_prefix": email_body_prefix,
        "email_recipient": "visa@debconf.org",
        "alert_text": "Email client or app will open with content generated from the form you've filled in. \
            Attach the following documents, then send the email to complete your request.\n \
            1. Copy of your passport\n \
            2. Travel insurance policy - In case you requested letter of guarantee and signed up for it already",
        "forms": [VisaRequestPassportDetailsForm, VisaRequestInviteeDetailsForm, VisaRequestTravelDetailsForm, VisaRequestGuaranteeDetailsForm]}
    return render(request, "forms/email-send-form.html", context)


def prometheus_metrics(request):
    User = get_user_model()
    accounts = User.objects.filter(is_active=1).count()
    completed_register_steps = defaultdict(int)
    confirmed = 0
    arrived = 0
    departed = 0
    for attendee in Attendee.objects.all():
        if attendee.confirmed():
            confirmed += 1
        completed_register_steps[attendee.completed_register_steps] += 1
        if attendee.arrived:
            arrived += 1
            if attendee.check_in.checked_out:
                departed += 1

    invoiced = defaultdict(Decimal)
    [invoiced[k] for k in ('new', 'paid', 'refunded', 'canceled')]
    for invoice in Invoice.objects.all():
        invoiced[invoice.status] += invoice.total

    travel_bursaries = defaultdict(int)
    [travel_bursaries[k] for k in ('submitted', 'accepted', 'pending', 'denied')]
    for bursary in Bursary.objects.all():
        if bursary.request_travel:
            travel_bursaries[bursary.travel_status] += bursary.travel_bursary

    queue_lengths = {}
    for queue in Queue.objects.all():
        queue_lengths[slugify(queue.name)] = queue.slots.count()

    metrics = [
        '# HELP accounts Total User accounts',
        '# TYPE accounts gauge',
        f'accounts {accounts}',
        '# HELP completed_register_step Completed Registration Step',
        '# TYPE completed_register_step gauge',
    ]
    for step in range(1, len(STEPS)):
        metrics.append(
            f'completed_register_step{{step="{step}"}} '
            f'{completed_register_steps[step]}')
    metrics += [
        '# HELP confirmed Confirmed Attendees',
        '# TYPE confirmed gauge',
        f'confirmed {confirmed}',
        '# HELP arrived Arrived Attendees',
        '# TYPE arrived gauge',
        f'arrived {arrived}',
        '# HELP departed Departed Attendees',
        '# TYPE departed gauge',
        f'departed {departed}',
        f'# HELP invoiced Invoiced Money (in {settings.DEBCONF_BILLING_CURRENCY})',
        '# TYPE invoiced gauge',
    ] + [
        f'invoiced{{status="{k}"}} {v}' for k, v in invoiced.items()
    ] + [
        f'# HELP travel_bursary Travel Bursary Money (in {settings.DEBCONF_BURSARY_CURRENCY})',
        '# TYPE travel_busary gauge',
    ] + [
        f'travel_bursary{{status="{k}"}} {v}' for k, v in travel_bursaries.items()
    ] + [
        '# HELP queue_length Registration Queue Length',
        '# TYPE queue_length gauge',
    ] + [
        f'queue_length{{queue="{k}"}} {v}' for k, v in queue_lengths.items()
    ] + [
        '',
    ]

    return HttpResponse("\n".join(metrics))
