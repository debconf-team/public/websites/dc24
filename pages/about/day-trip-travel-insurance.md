---
name: Travel Insurance Policy
---
### Traveler Insurance Policy (Accidents)

**Policy Duration:**  
From: 2024.07.31 00:00  
To: 2024.07.31 24:00  
**Insured Amount:** 60,920,000,000 KRW  
**Policy Number:** RQ24-40405747  
**Company:** doubleO Co., Ltd. (171-86-02420)  
**Premium:** 548,290 KRW (Single payment)  
**Group Contract:** 548,290 KRW  
**Travel Type:** General tourism | Nationwide coverage  

**Number of Insured Persons:** 200

#### Benefits:
- **Hospitalization or Outpatient Treatment due to Accident During Domestic Travel:**  
  - **Compensation:** 80% of the medical expenses for hospitalization and the difference after the deductible for outpatient treatment.  
  - **Deductible:** 20% of the medical expenses for hospitalization, and either 10,000-20,000 KRW or 20% of the medical expenses for outpatient treatment, whichever is higher.
  - **Maximum per Person:** 30,000,000 KRW

  **Exclusions:**  
  1. Intentional self-harm by the insured.  
  2. Intentional harm to the insured by the beneficiary.  
  3. Intentional harm to the insured by the policyholder.  
  4. Treatment related to pregnancy, childbirth (including caesarean section), and postnatal care.  
  5. Incidents caused by war, military actions, revolution, civil unrest, and riots (refer to policy terms for details).

- **Accidental Death During Domestic Travel:**  
  - **Compensation:** The agreed insurance amount will be paid if the insured dies as a direct result of an accident during domestic travel. (Not applicable to minors under 15, mentally incapacitated, or mentally deficient individuals.)
  - **Maximum per Person:** 100,000,000 KRW

  **Exclusions:**  
  1. Intentional harm by the insured, beneficiary, or policyholder.  
  2. Treatment related to pregnancy, childbirth, and postnatal care.  
  3. Incidents caused by war, military actions, revolution, civil unrest, and riots.  
  4. Accidents occurring during professional climbing or other activities related to the insured’s job, profession, or hobby (refer to policy terms for details).

- **Disability Due to Accident During Domestic Travel:**  
  - **Compensation:** The agreed insurance amount will be paid according to the disability rating table specified in the policy terms if the insured sustains a disability due to an accident during domestic travel.
  - **Maximum per Person:** 100,000,000 KRW

  **Exclusions:**  
  1. Intentional harm by the insured, beneficiary, or policyholder.  
  2. Treatment related to pregnancy, childbirth, and postnatal care.  
  3. Incidents caused by war, military actions, revolution, civil unrest, and riots.  
  4. Accidents occurring during professional climbing or other activities related to the insured’s job, profession, or hobby (refer to policy terms for details).

- **Non-Covered Medical Treatment Due to Accident During Domestic Travel:**  
  - **Compensation:** 70% of the non-covered medical expenses for hospitalization, and the difference after the deductible for outpatient treatment.  
  - **Deductible:** 30% of the non-covered medical expenses for hospitalization, and either 30,000 KRW or 30% of the medical expenses for outpatient treatment, whichever is higher.
  - **Maximum per Person:** 30,000,000 KRW

  **Exclusions:**  
  1. Intentional harm by the insured, beneficiary, or policyholder.  
  2. Treatment related to pregnancy, childbirth (including caesarean section), and postnatal care.  
  3. Incidents caused by war, military actions, revolution, civil unrest, and riots (refer to policy terms for details).

- **Death or Disability Due to Illness During Travel:**  
  - **Compensation:** The agreed insurance amount will be paid if the insured dies due to illness or sustains a disability with an 80% or higher disability rating according to the disability rating table specified in the policy terms.
  - **Maximum per Person:** 10,000,000 KRW

  **Exclusions:**  
  1. Intentional harm by the insured, beneficiary, or policyholder.  
  2. Treatment related to pregnancy, childbirth, and postnatal care.  
  3. Incidents caused by war, military actions, revolution, civil unrest, and riots.

- **Three Major Non-Covered Treatments During Domestic Travel:**  
  - **Compensation:** Up to 3,500,000 KRW for a maximum of 50 sessions for physical therapy, extracorporeal shock wave therapy, and proliferative therapy. Up to 2,500,000 KRW for a maximum of 50 sessions for injection therapy. Up to 3,000,000 KRW for MRI.  
  - **Deductible:** 30,000 KRW per session or 30% of the medical expenses, whichever is higher.
  - **Maximum per Person:** 3,500,000 KRW for physical therapy, extracorporeal shock wave therapy, and proliferative therapy; 2,500,000 KRW for injection therapy; 3,000,000 KRW for MRI.

  **Exclusions:**  
  1. Intentional harm by the insured, beneficiary, or policyholder.  
  2. Treatment related to pregnancy, childbirth (including caesarean section), and postnatal care.  
  3. Incidents caused by war, military actions, revolution, civil unrest, and riots (refer to policy terms for details).

- **Legal Liability for Third-Party Injuries or Property Damage During Travel:**  
  - **Compensation:** Covers legal liability for third-party bodily injuries or property damage resulting from an accidental incident during travel, with a 10,000 KRW deductible.
  - **Maximum per Person:** 30,000,000 KRW

  **Exclusions:**  
  1. Liability directly caused by the insured’s job performance.  
  2. Liability arising from the ownership, use, or management of properties solely for business purposes.  
  3. Liability arising from the ownership, use, or management of real estate by the insured.  
  4. Liability for injuries to the insured’s employees while performing work-related duties (refer to policy terms for details).

- **Loss or Damage to Personal Belongings During Travel:**  
  - **Compensation:** Covers loss or damage to personal belongings due to theft or accident, with a 10,000 KRW deductible.
  - **Maximum per Person:** 1,000,000 KRW

  **Exclusions:**  
  1. Intentional or gross negligence by the policyholder or the insured.  
  2. Losses caused intentionally by a relative or employee traveling with the insured.  
  3. Confiscation, requisition, or destruction by government or public authorities.  
  4. Losses due to defects in the insured item (refer to policy terms for details).

- **Food Poisoning During Domestic Travel:**  
  - **Compensation:** The agreed insurance amount will be paid if the insured is hospitalized for at least two days and receives treatment from a doctor due to food poisoning caused by consuming food during domestic travel.
  - **Maximum per Person:** 100,000 KRW

#### Important Notes:
1. This document is provided to help customers understand the policy terms, coverage amounts, and premiums. For detailed information, please refer to the policy terms and product explanation.
2. Insurance conditions and coverage amounts are based on the information provided by the customer. Changes in conditions and amounts may affect the insurance rates and premiums.
3. If the policy is subject to underwriting, the insurance conditions and other details may be restricted or changed based on the underwriting results.
4. For further inquiries, please contact the sales representative listed below.

**Sales Representative Contact Information:**
Customer Call Center: 1544-0114  
Website: [www.kbinsure.co.kr](http://www.kbinsure.co.kr)  
Mobile: [m.kbinsure.co.kr](http://m.kbinsure.co.kr)  
Corporate Sales Department 2, Branch 1 | Corporate Support Insurance Agency | Sales Agent: Kim Sang-min | Phone: 010-6394-6573
