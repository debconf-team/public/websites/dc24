---
name: Key-signing party
---

# Key-signing party

As usual at DebConf, there will be not be a single keysigning session, but
rather a continuous OpenPGP (pgp/gpg) keysigning, throughout the conference.

There will be an information session, introducing people to the keysigning
protocol, and publicly announcing the hash of the keysigning list. After that,
attendees are encouraged to keysign with each other, ad-hoc, throughout the
conference.

## How to participate

When registering for the conference, provide the fingerprints of any keys you
wish to include in the keysigning, in the field provided on the registration
form. If they aren't on public keyservers, upload them to a keyserver of your
choice (e.g. `keyserver.ubuntu.com`).

If you do this before registration closes, your fingerprint will be included in
the list collected by the conference organisers. Keys will be downloaded from
the public keyserver network and Debian Keyring.

This list will then be announced to all participants by email close to the
conference.

Before the conference, you should:

- Download this list at home.
- Verify that the keys you provided are present, and the fingerprints of them
  are correct.
-  Calculate the SHA256 hash of the list, e.g. `sha256sum list.txt`.

Bring with you to the conference:

- A print-out of the list, for note-taking when you want to sign people's keys.
- The SHA256 hash of the list.

At the information session, you can confirm that the hash of the list you're
working from is the same as everyone else's. When you want to keysign with
another attendee, you'll want to verify that they:

- Have verified the hash they generated, in the information session.
- Have verified their own key's fingerprint in the file.

If both of you have done these steps, they have the correct fingerprint of your
key and you have the correct fingerprint of theirs.

Given these assurances, you should be able to trust the fingerprint you have
for them, in your list, when you sign their key.

We recommend the use of caff (in the [debian signing-party
package](https://packages.debian.org/signing-party)) to sign keys. You can do
this once you get home from the conference, on a machine you trust, without any
peer pressure to sign keys. At the conference, all you need to do is take a
note that you intend to sign someone's key, when you get home.
