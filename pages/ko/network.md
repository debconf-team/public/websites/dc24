---
name: 네트워크
---

# 네트워크

DebConf24 의 네트워크는 대한민국의 다음 두 [국가연구교육망(NREN)](https://wiki.kreonet.net/pages/viewpage.action?pageId=184124074)이 제공합니다.

| 네트워크 | AS | 대역폭 | 비고 |
|--------|----|------|------|
| [KREONET(크레오넷)](https://kreonet.net) | [1237](https://bgp.tools/as/1237) | 100Gbps | |
| [KOREN(코렌)](https://www.koren.kr) | [9270](https://bgp.tools/as/9270) | 100Gbps | KOREN 부산 [PoP](https://ko.wikipedia.org/wiki/상호_접속_위치)이 행사장인 부경대 대연캠퍼스임 |

국가연구교육망(NREN, National Research and Education Network)은 연구 교육 목적의 인터넷으로 다른 NREN과의 상호 연결을 통해 연구와 교육을 진흥을 위해 운영됩니다.

